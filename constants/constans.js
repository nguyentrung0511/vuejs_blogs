export const BASE_URL = 'http://localhost:8000/api';

export const LIST_CATEGORY = [
    {categoryId: 0, name: "Pháp Luật"},
    {categoryId: 1, name: "Xã Hội"},
    {categoryId: 2, name: "Văn Hóa"},
    {categoryId: 3, name: "Nghệ Thuật"},
    {categoryId: 4, name: "Khoa Học"},
    {categoryId: 5, name: "Tự Nhiên"},
    {categoryId: 6, name: "Giải Trí"},
    {categoryId: 7, name: "Thể Thao"},
    {categoryId: 8, name: "Góc Nhìn"},
    {categoryId: 9, name: "Thời Sự"},
    {categoryId: 10, name: "Giáo Dục"},
    {categoryId: 11, name: "Sức Khỏe"},
    {categoryId: 12, name: "Số Hóa"},
]

export const LIST_POSITION = [
    {positionId: 1, positionName: "Việt Nam"},
    {positionId: 2, positionName: "Châu Á"},
    {positionId: 3, positionName: "Châu Âu"},
    {positionId: 4, positionName: "Châu Mỹ"},
]
